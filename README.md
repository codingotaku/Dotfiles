# Dotfiles

This repository contains some of my dotfiles for my Gentoo GNU/Linux running pure Wayland.

## How to install?

After a minimal Gentoo installation, follow the steps from 0 to 2 below.


**WARNING:** Running a random script found from online comes with the risk of distroying a perfectly usable system, so make sure to look at the scripts, and read this entire `README` file for easy troubleshooting.

### Step 0: Install the packages you need

- [packages/gentoo-make.conf](packages/gentoo-make.conf) contains my `make.conf` file, you will probably want to update it (like the `VIDEO_CARDS` and `MAKEOPTS`)
- [packages/gentoo-useflags.conf](packages/gentoo-packages.conf) contains the relevant useflags (there might be a few extra flags that sneaked in from my config), copy them and arrange them however you like.
- [packages/gentoo-packages.txt](packages/gentoo-packages.txt) contains list of packages to I use for this dotfiles.
- [packages/eselect-repo.conf](packages/eselect-repo.conf) contains all the repositories that I use.

Read the instructions on each of those packages and repositories to be sure that this is what you want to do.

For example, Keyboard driven tools like _sway_, _mpv_, _swayimg_, etc., are not something regular users are comfortable with using. If this is you, then **click away**.

Go through all the packags and install the ones you want (preferably all of them).

### Step 1: backup the configurations

The script in `Step 2` will **try** to backup your `.config` directory and other files it tries to copy, these will be present in the `.config-backup` directory in your `$HOME` directory.
But you will loose the changes from this backup if you run the script a second time, so if the script fails, take a copy of the `.config-backup`.

To avoid potential loss, take your config **backup NOW!**

### Step 2: Use the new configurations

Run the following command on a terminal. This requires `bash`, `curl`, and `git` installed in your system, usually they are pre-installed in all GNU/Linux distributions, if not, install them using the package manager of your GNU/Linux distribution.

```bash
curl -Lks https://codeberg.org/codingotaku/dotfiles/raw/branch/main/config_setup | /bin/bash
```

### Step 3: Profit!

Make sure to edit my configs and make it your own!

## Updates

There is no auto update for the setup, but you can run the following command to update the config files.

**This will replace your existing configurations! Don't worry, they are backed up to `.config-backup` directory!**

```bash
bash config_setup
```

_tip: use `bash config_setup ssh` if you have a codeberg account with ssh configured._

_extra tip: you can also do `config pull` if you have not modified any of the config files! Run `config status` to see if there are any changes_

## Screenshots

![screenshot](.dot-screenshots/blank.png)
![screenshot](.dot-screenshots/screenshot.png)

## Troubleshooting

### Missing icons in the launcher (fuzzel)

Install [Tela-circle-dracula-dark]() icon theme. Or update your preffered icon theme in the following files.

- [fuzzel.ini](.config/fuzzel/fuzzel.ini)
- [settings.ini](.config/gtk-3.0/settings.ini)
- [qt5ct.conf](.config/qt5ct/qt5ct.conf)
- [qt6ct.conf](.config/qt6ct/qt6ct.conf)

### Not detecting the keyboard layout

I use the UK qwerty layout for this config, to use the layout you need, read `man 5 sway-input` and edit `$HOME/.config/sway/config.d/input.conf`.

### Games does not launch, **error: No available video device** or **unable to open display X**

Install sdl2 package, [.profile](.profile) has environment variables that could potentially solve this issue.  
If installing sdl2 does not work, check if the game use sdl2 or any other wayland supported libraries, else you might have to install `xwayland`, you will have to do this yourself.

### Electron apps doesn't launch

You will probably need to install some of the `X11` headers (because electron does not know how dynamic linking works).

After that, try runing the program with `eww` from terminal (`eww signal-desktop`). To run them through fuzzel or other menus, you will need to edit its `.desktop` file, read the archwiki on [Wayland#Electron](https://wiki.archlinux.org/title/Wayland#Electron) to know how to do this.

## FAQ

### How do I navigate?

Press `Superkey + Enter` to launch the terminal, and do `cat ~/.config/sway/config.d/keybindings.conf` to read the key combinations.

### Caps lock is not working
I remapped caps lock key to the control key as it's easier for me to reach, this can be changed by commenting/removing the below line from [.config/sway/config.d/input.conf](.config/sway/config.d/input.conf)

```
  xkb_options ctrl:nocaps
```


### How to connect to Wi-Fi?

I Use `iwctl` to connect to Wi-Fi, use whatever you prefer, like `NetworkManager`. You may also need to update the `.config/scripts/status-scripts/wifi-status.sh` to get the SSID

### How to change the wallpaper?

The wallpaper can be configured in `~.config/sway/config.d/theme.conf` file.
The lockscreen Configurations must be done in `~/.config/swaylock/config` file

### I want to add my own configs but not track them

The git does not track any files that are not already present in this repo (run `config status` to see if you have modified any tracked files). So even if you create `$HOME/.config/sway/anotherfile` it wont be tracked.

For this sway Configuration, you can add your own config files under `$HOME/.config/sway/config.d` directory, the existing config will import all files present there.  
This is the best place to keep start-up programs, program specific configs, etc.

For bash variables, we are also reading the file `~/.user_vars` if it exits from [.bashrc](.bashrc), so create `~/.user_vars` file and add all your custom variables and imports there.

