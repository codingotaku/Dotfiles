#!/bin/env bash
################################################################################
## Some generally useful functions.

# Send an urgent notification with sound after timer ends
# Use message-timer <time> <"message">
message_timer() {
    sleep $1 && paplay /usr/share/sounds/freedesktop/stereo/complete.oga && notify-send -u critical "$2" && flite -t "$2"
}

# Run electron programs
eww() {
    echo "Running $1 With Electron Wayland Workaround (eww)"
    $1 --enable-features=UseOzonePlatform --ozone-platform=wayland
}

# Return color for common git status
git_color() {
    local git_status="$(git status 2>/dev/null)"

    if [[ !$git_status =~ "working directory clean" ]]; then
        echo "${RED} "
    elif [[ !$git_status =~ "have diverged" ]]; then
        echo "${RED} "
    elif [[ !$git_status =~ "fix conflicts" ]]; then
        echo "${RED} "
    elif [[ $git_status =~ "Your branch is ahead of" ]]; then
        echo "${PURPLE}↑ "
    elif [[ $git_status =~ "Your branch is behind" ]]; then
        echo "${YELLOW} "
    elif [[ $git_status =~ "Changes to be committed" || $git_status =~ "Changes not staged for commit" ]]; then
        echo "${YELLOW} "
    elif [[ $git_status =~ "untracked files present" ]]; then
        echo "${PURPLE}… "
    else
        echo "${GREEN} "
    fi
}

# return git branch name with color code
git_branch() {
    local branch=$(git branch --show-current 2>/dev/null)
    tmp=$(echo -e "$(git_color)$branch${BLUE}")
    echo -ne ${branch:+"──($tmp)"}
}

# return last command status with color code and symbol
exitstatus() {
    status=$?
    if [[ $status == 0 ]]; then
        echo -e "${GREEN} $status${BLUE}"
    else
        echo -e "${RED} $status${BLUE}"
    fi
}

# additional status for $PS1
generate_status() {
    status=$(exitstatus)
    echo -e "$(git_branch)──($status)"
}

osc7_cwd() {
    local strlen=${#PWD}
    local encoded=""
    local pos c o
    for ((pos = 0; pos < strlen; pos++)); do
        c=${PWD:$pos:1}
        case "$c" in
            [-/:_.!\'\(\)~[:alnum:]]) o="${c}" ;;
            *) printf -v o '%%%02X' "'${c}" ;;
        esac
        encoded+="${o}"
    done
    printf '\e]7;file://%s%s\e\\' "${HOSTNAME}" "${encoded}"
}

PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }osc7_cwd

copy_files() {
    if [ "$*" != "" ]; then
        IFS='\n' echo "$*"
    else
        cat
    fi | xargs readlink -f |
        awk '{ print "file://" $0 }' |
        wl-copy -t text/uri-list
}

gpl() {
    current=$(git branch --show-current)
    git pull --rebase origin $current
}

gpu() {
    current=$(git branch --show-current)
    git push origin $current
}
