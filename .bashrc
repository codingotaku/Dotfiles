#
# ~/.bashrc
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return
[[ "$(whoami)" = "root" ]] && return

# limits recursive functions, see 'man bash'
[[ -z "$FUNCNEST" ]] && export FUNCNEST=100

# User Prompts
BLACK="\e[1;30m"
RED="\e[1;31m"
GREEN="\e[1;32m"
YELLOW="\e[1;33m"
BLUE="\e[1;34m"
PURPLE="\e[1;35m"
CYAN="\e[1;36m"
WHITE="\e[1;37m"
RESET="\e[0m"

PS1="\n\[${BLUE}\]┌──(\[${PURPLE}\]\u\[${BLUE}\]@\[${PURPLE}\]\h\[${BLUE}\])──(\[${WHITE}\]\w\[${BLUE}\])──(\[${CYAN}\]\t\[${BLUE}\])\$(generate_status)\n\[${BLUE}\]└─> \[${YELLOW}\]\$ \[${RESET}\]"
PS2="\[${BLUE}\]……> \[${RESET}\]"
#------------------------------------------------------------

if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
    function clear() {
        vterm_printf "51;Evterm-clear-scrollback"
        tput clear
    }
fi

# no bells
set bell-style none

# Bindings, Aliases and Functions
source ~/.profile
source ~/.bash_aliases
source ~/.bash_functions
[ -f ~/.user_vars ] && source ~/.user_vars
