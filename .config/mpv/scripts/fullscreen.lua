--makes mpv change background color to black when in full screen
--please note that the default background color here should be set to match the one in config file

mp.observe_property("fullscreen", "bool", function(name, value)
    local color = mp.get_property_native("background")
    if value then
        mp.set_property_native("background-color", '#000000')
    else
        mp.set_property_native("background-color", '#282a36')
    end
end)
