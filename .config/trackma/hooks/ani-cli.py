"""
 Original Author: imsamuka
 Updated by:
     - codingotaku <contact@codingotaku.com?
 Place under ~/.config/trackma/hooks/ or ~/.trackma/hooks/

 When an episode is not found, it executes 'ani-cli' to find and
 watch it via streaming automatically!

 Last tested with ani-cli version 4.9.2
"""

from subprocess import Popen, PIPE, DEVNULL
import re


# Executed when trying to watch an episode that doesn't exist in your library
def episode_missing(engine, show, episode):
    anicli = "/home/otaku/applications/ani-cli/ani-cli"

    pattern_to_remove = r"\(?TV\)?|\(?ONA\)?|\(?OVA\)?|(\d+)(?:nd|rd|th|st)"
    query = re.sub(pattern_to_remove, '\\1', show["title"]).strip()
    args = [
        anicli, "--no-detach", "--exit-after-play", "-q", "best", "-e",
        str(episode), query
    ]

    cmd = " ".join(args[:-1]) + f" '{query}'"
    engine.msg.info("running anicli", cmd)  # Show the command used
    process = Popen(args,
                    stdin=PIPE,
                    stdout=DEVNULL,
                    stderr=DEVNULL,
                    text=True)
    process.communicate(input="q")
