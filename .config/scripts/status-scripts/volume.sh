#!/bin/env bash

is_muted=$(pactl get-sink-mute @DEFAULT_SINK@ 2>/dev/null)
volume=$(pactl get-sink-volume @DEFAULT_SINK@ 2>/dev/null | head -n -1 | cut -d/ -f2)
volume="${volume//[[:space:]]/}"

[ -z "$volume" ] && exit

declare -A volume_icons=([4]='󰝝' [3]='' [2]='󰕾' [1]='󰖀' [0]='󰕿')

get_icon() {
    local volume_val=${volume::-1}
    local key=$(("$volume_val" / 25))
    [[ key -gt 3 ]] && key=4
    printf '%s' "${volume_icons[$key]}"
}

if [[ $is_muted =~ 'yes' ]]; then
    printf '<span background="#ff5555"> Muted (%s) 󰝟 </span>' "$volume"
else
    printf '%s   Volume %s' "$(get_icon)" "$volume"
fi
