#!/usr/bin/env sh

DOT_VARS="/tmp/dot-files/.vars"
BAT_FILE="$DOT_VARS/bat"
BAT_STATE="$DOT_VARS/bat_state"
BAT_ALERT_ID="$DOT_VARS/bat_alert_id"

test -d "$DOT_VARS" || mkdir -p $DOT_VARS

echo 0 >"$BAT_FILE"
echo 0 >"$BAT_STATE"
echo 0 >"$BAT_ALERT_ID"
