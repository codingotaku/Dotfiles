#!/usr/bin/env sh

# Displays a progress overlay for the category and value
# $1 category default=progress
# $2 value optional, required if category is not brightness or volumn

category="${1:-progress}"
value=$2

[[ $category = 'brightness' ]] && value=$(brightnessctl --machine-readable | cut -d, -f4)
[[ $category = 'volumn' ]] && value=$(pactl get-sink-volume @DEFAULT_SINK@ | head -n 1 | awk '{print substr($5, 1, length($5)-1)}')

DOT_VARS="/tmp/dot-files/.vars"
PROGRESS_ID="$DOT_VARS/$category-progress"

[[ -f $PROGRESS_ID ]] || echo 0 >"$PROGRESS_ID"

prev_id=$(cat "$PROGRESS_ID")

id=$(notify-send --urgency=low --replace-id="$prev_id" --print-id --app-name 'Progress' -t 3000 --hint string:syncronous:$category --hint int:value:$value "$category $value")

[[ -n "$id" ]] && echo "$id" >"$PROGRESS_ID"
