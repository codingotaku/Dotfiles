#!/usr/bin/env sh

laptop='eDP'

if grep -q open /proc/acpi/button/lid/LID/state; then
    swaymsg output "$laptop" enable
else
    swaymsg output "$laptop" disable
fi
